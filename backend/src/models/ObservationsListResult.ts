import Observation from "./Observation";

export default class ObservationsListResult {
    
    page: number = 0;
    itemsPerPage: number = 20;
    totalCount: number = 0;
    totalPages: number = 0;
    public records: Observation[] = [];
    
}