import { injectable } from "inversify";
import { getConnection } from "../config/database";
import Observation from "../models/Observation";
import camelize from 'camelcase-keys';
import ObservationsListResult from "../models/ObservationsListResult";

@injectable()
export default class ObservationRepository {

    public async findAllObservations(page: number, itemsPerPage: number): Promise<ObservationsListResult> {

        const conn = await getConnection();

        const pageOffset = (page - 1) * itemsPerPage;
        const observationsResult = await conn.query(QUERY.FIND_ALL_OBSERVATIONS, { pageOffset, itemsPerPage });
        const observations = (observationsResult[0] as [] || []).map((observation: any) => camelize(observation.payload) as Observation);


        const observationsCountResult = await conn.query(QUERY.COUNT_ALL_OBSERVATIONS);
        const observationsCount = (observationsCountResult[0] as any)[0].count as number;

        const observationsListResult: ObservationsListResult = {
            page,
            itemsPerPage,
            records: observations,
            totalCount: observationsCount,
            totalPages: observationsCount / itemsPerPage

        }

        conn.release();


        return observationsListResult;
    }

}


const QUERY = {
    FIND_ALL_OBSERVATIONS: "SELECT payload FROM events WHERE event_type IN ('concern_raised','catheter_observation','food_intake_observation','fluid_intake_observation','physical_health_observation','mood_observation') ORDER BY timestamp DESC LIMIT :itemsPerPage OFFSET :pageOffset",
    COUNT_ALL_OBSERVATIONS: "SELECT count(id) as count FROM events WHERE event_type IN ('concern_raised', 'catheter_observation', 'food_intake_observation', 'fluid_intake_observation','physical_health_observation', 'mood_observation')"
}