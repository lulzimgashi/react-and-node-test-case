import { inject, injectable } from "inversify";
import { DI_TYPES } from "../config/di-types";
import ObservationRepository from "../repositories/observation-repository";

@injectable()
export default class ObservationService {

    @inject(DI_TYPES.ObservationRepository) private observationRepository!: ObservationRepository;

    public async getAllObservations(page: number, itemsPerPage: number) {

        const observations = await this.observationRepository.findAllObservations(page, itemsPerPage);

        return observations;
    }

}