import { Request } from "express";
import { inject } from "inversify";
import { controller, httpGet } from "inversify-express-utils";
import { DI_TYPES } from "../config/di-types";
import ObservationsListResult from "../models/ObservationsListResult";
import ObservationService from "../services/observation-service";

@controller('/observations')
class ObservationsController {

  @inject(DI_TYPES.ObservationService) private observationService!: ObservationService;

  @httpGet('/')
  public async getObservations(request: Request): Promise<ObservationsListResult> {

    const itemsPerPage = 20;
    const page = parseInt(request.query.page as string || '1', 0) || 1;
    

    const observations = await this.observationService.getAllObservations(page, itemsPerPage);

    return observations;
  }

}

export default ObservationsController;