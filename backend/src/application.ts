import { InversifyExpressServer } from 'inversify-express-utils';
import { myContainer } from './config/inversify.config';
import "./controllers/observations-controller";
import * as bodyParser from 'body-parser';
import cors from 'cors';

const server = new InversifyExpressServer(myContainer);

server.setConfig((app) => {
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    app.use(cors());
});


export default server.build();
