
export const DI_TYPES = {
    // repos
    ObservationRepository: Symbol.for("ObservationRepository"),
    // services
    ObservationService: Symbol.for("ObservationService")
}

