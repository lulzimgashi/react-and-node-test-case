import { Container } from 'inversify';
import ObservationRepository from '../repositories/observation-repository';
import ObservationService from '../services/observation-service';
import { DI_TYPES } from './di-types';

const myContainer = new Container();
myContainer.bind(DI_TYPES.ObservationRepository).to(ObservationRepository);
myContainer.bind(DI_TYPES.ObservationService).to(ObservationService);

export { myContainer };