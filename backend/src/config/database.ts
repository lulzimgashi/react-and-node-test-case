// tslint:disable-next-line:no-submodule-imports
import { createPool } from 'mysql2/promise';

// read from env variables
const config = {
    host: 'host',
    user: 'user',
    password: 'password',
    database: 'database'
}

const pool = createPool(config);
const getConnection = async () => {
    const conn = await pool.getConnection()

    conn.config.queryFormat = (query, values) => {
        if (!values) return query;
        return query.replace(/\:(\w+)/g, (txt, key) => {
            if (values.hasOwnProperty(key)) {
                return escape(values[key]);
            }
            return txt;
        });
    };

    return conn;
}



export { getConnection } 