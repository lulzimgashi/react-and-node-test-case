// API Configurations
// const API_HOST_LOCAL = 'localhost:8000';
const API_HOST_DEV = 'birdie-backend-test.herokuapp.com';

let server = API_HOST_DEV;

export const API = `https://${server}`;