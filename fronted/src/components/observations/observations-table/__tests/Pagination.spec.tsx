import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createNewStoreInstance } from '../../../../store';
import { Provider } from 'react-redux';
import Pagination from '../Pagination';
import {  observationsReceivedAction } from '../../../../store/actions/observations-actions/observationActionCreator';
import { normalizedObservationsMock, normalizedObservationsMock2, observationsMetaDataMock } from '../../../../models/__mocks/observations';
import userEvent from '@testing-library/user-event';


describe("Test Table Pagination", () => {

    test("When there is no data on table", async () => {

        const store = createNewStoreInstance();
        render(
            <Provider store={store}>
                <Pagination />
            </Provider>
        )

        expect(await screen.findByTestId("table-pagination")).toBeEmptyDOMElement()

    });

    test("If pagination buttons are rendered correctly", async () => {

        const store = createNewStoreInstance();
        store.dispatch(observationsReceivedAction(normalizedObservationsMock, observationsMetaDataMock))


        render(
            <Provider store={store}>
                <Pagination />
            </Provider>
        )

        expect(await screen.findAllByTestId("table-pagination-button")).toHaveLength(observationsMetaDataMock.totalPages)

    });

    test("Pagination button functionality", async () => {

        const store = createNewStoreInstance();
        store.dispatch(observationsReceivedAction(normalizedObservationsMock, observationsMetaDataMock))

        const { rerender } = render(<Provider store={store}><Pagination /></Provider>)

        let paginationButtons = await screen.findAllByTestId("table-pagination-button");
        expect(paginationButtons[0]).toHaveAttribute("data-is-selected", "true");


        store.dispatch(observationsReceivedAction(normalizedObservationsMock2, { ...observationsMetaDataMock, page: 2 }))
        expect(paginationButtons[0]).toHaveAttribute("data-is-selected", "false");
        expect(paginationButtons[1]).toHaveAttribute("data-is-selected", "true");

        userEvent.click(paginationButtons[0]);
        rerender(<Provider store={store}><Pagination /></Provider>)

        expect(paginationButtons[0]).toHaveAttribute("data-is-selected", "true");
        expect(paginationButtons[1]).toHaveAttribute("data-is-selected", "false");

    });

})