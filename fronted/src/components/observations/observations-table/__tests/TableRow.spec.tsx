import * as React from 'react';
import { render, screen } from '@testing-library/react';
import TableRow from '../TableRow';

describe('Test TableRow', () => {

    test('If has correct styles when used as Header', () => {
        const { container } = render(<TableRow isHeader={true} />);

        expect(container.firstChild).toHaveStyle({
            'font-size': '16px',
            'font-weight': '600'
        })

    });

    test('If has correct styles when used as Row', () => {
        const { container } = render(<TableRow />);

        expect(container.firstChild).toHaveStyle({
            'font-size': '14px',
            'font-weight': '400'
        })

    });

    test('If renders child correctly', async () => {
        render(
            <TableRow>
                <div data-testid="custom-element">Timestamp</div>
            </TableRow>
        )

        expect(await screen.findByTestId("custom-element")).not.toBeNull();
    })

});