import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createNewStoreInstance } from '../../../../store';
import { Provider } from 'react-redux';
import ObservationsTable from '../ObservationsTable';
import { normalizedObservationsMock, observationsMetaDataMock } from '../../../../models/__mocks/observations';
import { observationsReceivedAction } from '../../../../store/actions/observations-actions/observationActionCreator';


describe("Test Observations Table", () => {


    test("If there are no rows to display", async () => {

        const store = createNewStoreInstance();

        render(
            <Provider store={store}>
                <ObservationsTable />
            </Provider>
        )

        expect(await screen.findByTestId("table-rows-container")).toBeEmptyDOMElement()

    });


    test("If displaying rows correctly", async () => {

        const store = createNewStoreInstance();
        store.dispatch(observationsReceivedAction(normalizedObservationsMock, observationsMetaDataMock))

        render(
            <Provider store={store}>
                <ObservationsTable />
            </Provider>
        )

        // 20 items + 1 header
        expect(await screen.findAllByTestId("table-row")).toHaveLength(21);

    });

});