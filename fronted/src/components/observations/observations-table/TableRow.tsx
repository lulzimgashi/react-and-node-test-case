// @flow
import * as React from 'react';
import styled from 'styled-components';

interface TableRowProps {
    isHeader?: boolean;
    children?: React.ReactNode;
}

const TableRowStyled = styled.div`
    width: 100%;
    display: flex;
    border-bottom: 1px solid lightgrey;
    min-height: 40px;
    font-size: ${(props: TableRowProps) => props.isHeader ? '16px' : '14px'};
    font-weight: ${(props: TableRowProps) => props.isHeader ? '600' : '400'};
`;

export default function TableRow(props: TableRowProps) {

    return (
        <TableRowStyled isHeader={props.isHeader} data-testid="table-row">
            {props.children}
        </TableRowStyled>
    );
}