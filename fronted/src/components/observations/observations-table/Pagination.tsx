import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import ObservationsMetaData from '../../../models/ObservationsMetaData';
import { fetchObservationsAction } from '../../../store/actions/observations-actions/observationActionCreator';
import { RootState } from '../../../store/reducers';

interface PaginationProps {
}

interface PageButtonProps {
    isSelected: boolean;
}

const PaginationStyled = styled.div`
    width: 100%;
    max-width: 100%;
    overflow-x: auto;
    box-sizing: border-box;
    padding: 10px 10px;
    display: flex;
    align-items: center;
    margin-top: 20px;
`;

const PageButtonStyled = styled.div`
    padding: 10px;
    border-radius: 4px;
    border: 1px solid lightgrey;
    font-weight: ${(props: PageButtonProps) => props.isSelected ? '600' : '400'};
    cursor: pointer;
    margin: 0 5px;

    &:hover {
        background-color: lightgrey;
    }
`;

export default function Pagination(props: PaginationProps) {

    const observationMetaData = useSelector((state: RootState) => state.observations.metaData);
    const dispatch = useDispatch();

    const goToPage = (pageNumber: number) => {

        const metaData = new ObservationsMetaData();
        metaData.page = pageNumber;

        dispatch(fetchObservationsAction(metaData));
    };

    const renderButtons = () => {

        const buttons: JSX.Element[] = [];

        let i = 1;
        while (i <= observationMetaData.totalPages) {

            const pageNumber = i;

            buttons.push(
                <PageButtonStyled
                    key={`pagination-button-${i}`}
                    data-testid="table-pagination-button"
                    data-is-selected={i === observationMetaData.page}
                    onClick={() => goToPage(pageNumber)}
                    isSelected={i === observationMetaData.page}
                >
                    {i}
                </PageButtonStyled>
            );

            i++;
        }

        return buttons;
    };

    return (
        <PaginationStyled data-testid="table-pagination">
            {renderButtons()}
        </PaginationStyled>
    );
}