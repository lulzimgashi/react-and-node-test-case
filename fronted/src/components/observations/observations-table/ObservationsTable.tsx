// @flow
import * as React from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import TableRow from './TableRow';
import TableRowsContainer from './TableRowsContainer';
import TableColumn from './TableColumn';
import Pagination from './Pagination';
import moment from 'moment';
import { RootState } from '../../../store/reducers';
import ObservationUtils from '../../../utils/ObservationUtils';

interface ObservationsTableProps {

}

const TableContainer = styled.div`
    width: 100%;
`;

export default function ObservationsTable(props: ObservationsTableProps) {

    const observations = useSelector((state: RootState) => state.observations);

    const renderTableRows = () => {

        const observationIds = observations.pages[observations.metaData.page] || [];

        return observationIds.map((observationId: string) => {
            const observation = observations.data[observationId];

            return (
                <TableRow key={observationId}>

                    <TableColumn>
                        {moment(observation.timestamp).format('LLL')}
                    </TableColumn>

                    <TableColumn>
                        {observation.eventType}
                    </TableColumn>

                    <TableColumn>
                        {ObservationUtils.getEventValue(observation)}
                    </TableColumn>

                </TableRow>
            );
        });
    };

    return (
        <TableContainer>
            <TableRow isHeader={true}>

                <TableColumn>
                    Timestamp
                </TableColumn>

                <TableColumn>
                    Event Type
                </TableColumn>

                <TableColumn>
                    Event Value
                </TableColumn>

            </TableRow>

            <TableRowsContainer>
                {renderTableRows()}
            </TableRowsContainer>

            <Pagination />
        </TableContainer>
    );

}
