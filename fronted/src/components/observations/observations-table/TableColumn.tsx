// @flow
import * as React from 'react';
import styled from 'styled-components';

interface TableColumnStyledProps {
    children?: React.ReactNode;
}

const TableColumnStyled = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export default function TableColumn(props: TableColumnStyledProps) {

    return (
        <TableColumnStyled>
            {props.children}
        </TableColumnStyled>
    );
}