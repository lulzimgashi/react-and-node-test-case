// @flow
import * as React from 'react';
import styled from 'styled-components';

interface TableRowsContainerProps {
    children?: React.ReactNode;
}

const TableRowsContainerStyled = styled.div`
    width: 100%;
`;

export default function TableRowsContainer(props: TableRowsContainerProps) {

    return (
        <TableRowsContainerStyled data-testid="table-rows-container">
            {props.children}
        </TableRowsContainerStyled>
    );
}