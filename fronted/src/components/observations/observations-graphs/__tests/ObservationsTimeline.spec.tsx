import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createNewStoreInstance } from '../../../../store';
import { observationsReceivedAction } from '../../../../store/actions/observations-actions/observationActionCreator';
import { normalizedObservationsMock, observationsMetaDataMock } from '../../../../models/__mocks/observations';
import { Provider } from 'react-redux';
import ObservationsTimeline from '../ObservationsTimeline';


describe("Test Observations Timeline", () => {

    test("If it's rendered correclty", async () => {

        const store = createNewStoreInstance();
        store.dispatch(observationsReceivedAction(normalizedObservationsMock, observationsMetaDataMock))

        render(
            <Provider store={store}>
                <ObservationsTimeline />
            </Provider>
        )

        expect(await screen.findAllByTestId('observations-timeline-data-point')).toHaveLength(20);

    })

})