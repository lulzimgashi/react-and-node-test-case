// @flow
import * as React from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';
import styled from 'styled-components';
import { RootState } from '../../../store/reducers';
import ObservationUtils from '../../../utils/ObservationUtils';

interface ObservationsTimelineProps {

}

interface TimelineDataPointProps {
    color: string;
}

const TimelineContainer = styled.div`
    height: 100%;
    display: flex;
    align-items: center;
    overflow-x: auto;
    max-width: 50%;
    box-sizing: border-box;
    padding: 20px;
    padding-bottom:80px;
    border-bottom: 5px solid ${ObservationUtils.generateRandomColor()};
`;

const TimelineDataPoint = styled.div`
    border-radius: 4px;
    background-color: ${(props: TimelineDataPointProps) => props.color};
    margin-right: 20px;
    box-sizing: border;
    padding: 15px;
    white-space: nowrap;
    position: relative;
    border: 1px solid lightgrey;

    &:after {
        position: absolute;
        content: "";
        min-width: 3px;
        min-height: 80px;
        background-color: ${(props: TimelineDataPointProps) => props.color};
        top: 100%;
        left: 50%;
        transform: translateX(-50%);
        z-index: 10;
    }
`;

export default function ObservationsTimeline(props: ObservationsTimelineProps) {

    const observations = useSelector((state: RootState) => state.observations);

    const renderDataPoints = () => {

        const observationIds = observations.pages[observations.metaData.page] || [];

        return observationIds.map((observationId: string) => {
            const observation = observations.data[observationId];
            const observationValue: string = ObservationUtils.getEventValue(observation) || 'other';

            return (
                <TimelineDataPoint
                    key={`timeline-data-point-${observationId}`}
                    color={ObservationUtils.generateRandomColor()}
                    data-testid="observations-timeline-data-point"
                >
                    <div>{moment(observation.timestamp).format('LLL')}</div>
                    <div>{observation.eventType}</div>
                    <div>{observationValue}</div>
                </TimelineDataPoint>
            );
        });

    };

    return (
        <TimelineContainer data-testid="observations-timeline">
            {renderDataPoints()}
        </TimelineContainer>
    );

}