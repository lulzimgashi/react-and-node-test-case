import * as React from 'react';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import variablePie from 'highcharts/modules/variable-pie.js';
import { useSelector } from 'react-redux';
import { RootState } from '../../../store/reducers';
import ObservationUtils from '../../../utils/ObservationUtils';

variablePie(Highcharts);

interface ObservationsPieChartProps {

}

const chartOptions: Highcharts.Options = {
    chart: {
        type: 'variablepie'
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
            'Count: <b>{point.y}</b><br/>' +
            'Event Type: <b>{point.eventType}</b><br/>'
    },
    title: undefined
};

export default function ObservationsPieChart(props: ObservationsPieChartProps) {

    // tslint:disable-next-line:no-any
    const chartRef = React.useRef<any>(null);
    const observations = useSelector((state: RootState) => state.observations);

    const getDataForChart = () => {
        const observationIds = observations.pages[observations.metaData.page] || [];

        const observationGroupByType: any = {};

        observationIds.forEach((observationId: string) => {
            const observation = observations.data[observationId];
            const observationValue: string = ObservationUtils.getEventValue(observation) || 'other';

            if (observationValue in observationGroupByType) {
                observationGroupByType[observationValue].y = observationGroupByType[observationValue].y + 1;
            } else {
                observationGroupByType[observationValue] = {
                    name: observationValue,
                    y: 1,
                    eventType: observation.eventType
                };
            }
        });

        return Object.keys(observationGroupByType).map((observationValueKey) => {
            return observationGroupByType[observationValueKey];
        });

    };

    React.useEffect(() => {

        const chartInstance = chartRef.current;

        if (chartInstance) {
            chartInstance.chart.update({
                series: [{
                    type: 'variablepie',
                    minPointSize: 30,
                    innerSize: '80%',
                    zMin: 0,
                    data: getDataForChart()
                }]
            });
        }
        
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [observations.metaData.page]);

    return (
        <HighchartsReact
            ref={chartRef}
            highcharts={Highcharts}
            options={chartOptions}
            {...props}
        />
    );

}