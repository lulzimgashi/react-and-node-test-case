import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import ObservationsMetaData from '../../models/ObservationsMetaData';
import { fetchObservationsAction } from '../../store/actions/observations-actions/observationActionCreator';
import { RootState } from '../../store/reducers';
import Loader from '../Loader';
import ObservationsPieChart from './observations-graphs/ObservationsPieChart';
import ObservationsTimeline from './observations-graphs/ObservationsTimeline';
import ObservationsTable from './observations-table/ObservationsTable';

interface ObservationsContainerProps {

}

const ObservationsContainerStyled = styled.div`
    width: 100%;
`;

const ObservationsContainerRowStyled = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin: 25px 0;
    border-bottom: 1px solid grey;
    box-sizing: border-box;
    padding-bottom: 20px;
`;

export default function ObservationsContainer(props: ObservationsContainerProps) {

    const isLoading = useSelector((state: RootState) => state.observations.isLoading);
    const observationLoadedOnce = useSelector((state: RootState) => state.observations.loadedOnce);
    const dispatch = useDispatch();

    React.useEffect(() => {
        if (!isLoading && !observationLoadedOnce) {
            const observationMetaData = new ObservationsMetaData();
            observationMetaData.page = 1;
            dispatch(fetchObservationsAction(observationMetaData));
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (isLoading) {
        return <Loader />;
    }

    return (
        <ObservationsContainerStyled>
            <ObservationsContainerRowStyled>
                <ObservationsPieChart />
                <ObservationsTimeline />
            </ObservationsContainerRowStyled>
            <ObservationsTable />
        </ObservationsContainerStyled>
    );

}
