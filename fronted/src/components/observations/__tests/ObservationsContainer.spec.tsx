import * as React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import ObservationsContainer from '../ObservationsContainer';
import { Provider } from 'react-redux';
import { createNewStoreInstance } from '../../../store';
import fetchMock from 'jest-fetch-mock';
import { observationsMock } from '../../../models/__mocks/observations';


describe('Test Observations Container', () => {

    beforeEach(() => {
        fetchMock.resetMocks();
    });

    test("If Loader is appearing correclty", async () => {

        render(
            <Provider store={createNewStoreInstance()}>
                <ObservationsContainer />
            </Provider>
        )

        expect(screen.queryByTestId('observations-loader')).not.toBeNull();

    });

    test("If Data is loaded correclty", async () => {

        fetchMock.mockResponseOnce(JSON.stringify(observationsMock));

        const store = createNewStoreInstance();
        render(
            <Provider store={store}>
                <ObservationsContainer />
            </Provider>
        )

        await waitFor(() => expect(fetchMock).toHaveBeenCalledTimes(1));

        expect(screen.queryByTestId('observations-loader')).toBeNull();

    });

});
