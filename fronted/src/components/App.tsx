// @flow
import * as React from 'react';
import styled from 'styled-components';
import ObservationsContainer from './observations/ObservationsContainer';

interface AppProps {

}

const AppContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  font-family: 'Arial';
`;

export default function App(props: AppProps) {

  return (
    <>
      <AppContainer>
        <ObservationsContainer />
      </AppContainer>
    </>
  );

}