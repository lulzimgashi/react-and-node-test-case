import ObservationAction from './observationAction';
import { ObservationActionType } from './observationActionTypes';
import Observation from './../../../models/Observation';
import ObservationsMetaData from '../../../models/ObservationsMetaData';
import { NormalizedModel } from '../../../models/NormalizedModel';

export const observationsReceivedAction = (data: NormalizedModel<Observation>, metaData: ObservationsMetaData) => {

    const ObservationReceivedAction: ObservationAction = {
        type: ObservationActionType.OBSERVATIONS_RECEIVED,
        payload: data,
        observationsMetaData: metaData
    };

    return ObservationReceivedAction;
};

export const fetchObservationsAction = (metaData: ObservationsMetaData) => {

    const FetchObservationAction: ObservationAction = {
        type: ObservationActionType.FETCH_OBSERVATIONS,
        observationsMetaData: metaData
    };

    return FetchObservationAction;

};

export const changeObservationsPageAction = (metaData: ObservationsMetaData) => {

    const ChangeObservatiosnPageActions: ObservationAction = {
        type: ObservationActionType.CHANGE_OBSERVATIONS_PAGE,
        observationsMetaData: metaData
    };

    return ChangeObservatiosnPageActions;
};