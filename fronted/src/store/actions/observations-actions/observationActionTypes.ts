export enum ObservationActionType {
    OBSERVATIONS_RECEIVED = '@@observation/observations_received',
    CHANGE_OBSERVATIONS_PAGE = '@@observation/change_observations_page',
    // saga action types
    FETCH_OBSERVATIONS = '@@observation/fetch_observations'
}