
import { Action } from 'redux';
import { NormalizedModel } from '../../../models/NormalizedModel';
import Observation from '../../../models/Observation';
import ObservationsMetaData from '../../../models/ObservationsMetaData';
import { ObservationActionType } from './observationActionTypes';

export default interface ObservationAction extends Action<ObservationActionType> {
    payload?: NormalizedModel<Observation>;
    observationsMetaData?: ObservationsMetaData;
}