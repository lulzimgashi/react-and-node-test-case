import { RootState } from '.';
import ObservationsMetaData from '../../models/ObservationsMetaData';

const initalState: RootState = {
    observations: {
        isLoading: false,
        data: {},
        loadedOnce: false,
        pages: {},
        metaData: new ObservationsMetaData()
    }
};

export default initalState;