import { combineReducers } from 'redux';
import ObservationsReducer, { ObservationsReducerType } from './observationsReducer';

export type RootState = Readonly<{
    observations: ObservationsReducerType
}>;

export const rootReducer = combineReducers<RootState>({
    observations: ObservationsReducer
});