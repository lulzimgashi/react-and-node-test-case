
import { NormalizedModel } from '../../models/NormalizedModel';
import Observation from '../../models/Observation';
import ObservationsMetaData from '../../models/ObservationsMetaData';
import ObservationAction from '../actions/observations-actions/observationAction';
import { ObservationActionType } from '../actions/observations-actions/observationActionTypes';
import initalState from './initialState';

export interface ObservationsReducerType {
    isLoading: boolean;
    data: NormalizedModel<Observation>;
    loadedOnce: boolean;
    pages: NormalizedModel<string[]>;
    metaData: ObservationsMetaData;
}

const observationsReducer = (state: ObservationsReducerType = initalState.observations, action: ObservationAction) => {


    switch (action.type) {
        case ObservationActionType.FETCH_OBSERVATIONS:

            return { ...state, isLoading: true };

        case ObservationActionType.OBSERVATIONS_RECEIVED:

            const observationIds = Object.keys(action.payload as NormalizedModel<Observation>);

            return {
                ...state,
                isLoading: false,
                loadedOnce: true,
                data: { ...state.data, ...(action.payload as NormalizedModel<Observation>) },
                pages: {
                    ...state.pages,
                    [action.observationsMetaData?.page || 0]: observationIds
                },
                metaData: action.observationsMetaData || state.metaData
            };

        case ObservationActionType.CHANGE_OBSERVATIONS_PAGE:

            state.metaData.page = action.observationsMetaData?.page || state.metaData.page;
            state.isLoading = false;
            return { ...state };

        default:
            return state;
    }
};


export default observationsReducer;