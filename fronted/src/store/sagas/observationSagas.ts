import { put, select } from 'redux-saga/effects';
import ObservationAction from '../actions/observations-actions/observationAction';
import {
    changeObservationsPageAction,
    observationsReceivedAction
} from '../actions/observations-actions/observationActionCreator';
import { normalize } from 'normalizr';
import ObservationsMetaData from '../../models/ObservationsMetaData';
import ObservationsListResult from '../../models/ObservationsListResult';
import { API } from '../../config/api';
import { ObservationSchema } from '../../models/Observation';

export function* fetchObservationsFromAPI(action: ObservationAction) {

    const pages = (yield select()).observations.pages;

    if (action.observationsMetaData && action.observationsMetaData.page in pages) {

        yield put(changeObservationsPageAction(action.observationsMetaData));

        return;
    }

    let observations = {};
    let observationsMetaData = new ObservationsMetaData();

    try {
        const response = yield fetch(API + `/observations?page=${action.observationsMetaData?.page || 1}`);
        if (response.ok) {

            const observationsListResult: ObservationsListResult = yield response.json();

            observations = normalize(observationsListResult.records, [ObservationSchema]).entities.observations || {};

            delete observationsListResult.records;
            observationsMetaData = { ...observationsListResult };

        }
    } catch (e) {
        // TODO : handle error
    }

    yield put(observationsReceivedAction(observations, observationsMetaData));

}
