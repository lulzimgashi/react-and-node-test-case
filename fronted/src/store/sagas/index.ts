import { all, takeLatest } from 'redux-saga/effects';
import { ObservationActionType } from '../actions/observations-actions/observationActionTypes';
import { fetchObservationsFromAPI } from './observationSagas';

function* initSaga() {
  yield all([actionWatch()]);
}

function* actionWatch() {
  yield takeLatest(ObservationActionType.FETCH_OBSERVATIONS, fetchObservationsFromAPI);
}

export default initSaga;