import { applyMiddleware, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import { rootReducer } from './reducers';
import initSaga from './sagas';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: () => undefined;
    // tslint:disable-next-line:no-any
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: (arg: any) => undefined;
  }
}

export const history = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const createNewStoreInstance = () => {

  const newSagaMiddleware = createSagaMiddleware();

  const newStore = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(newSagaMiddleware)),
  );

  newSagaMiddleware.run(initSaga);
  return newStore;
}

const store = createNewStoreInstance()

export default store;