import { NormalizedModel } from "../NormalizedModel"
import Observation from "../Observation"
import ObservationsMetaData from "../ObservationsMetaData"

export const observationsMock = {
    "page": 1,
    "itemsPerPage": 20,
    "records": [
        {
            "id": "19f90822-d91f-4c5f-a67c-38e5c87f7e27",
            "meal": "snack",
            "note": "3 x biscuits ",
            "visitId": "b7c54214-8861-4495-a244-fac62abace70",
            "timestamp": "2019-05-12T22:05:07+01:00",
            "eventType": "food_intake_observation",
            "caregiverId": "56890b44-f575-4d66-840a-b402d19a81e5",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        },
        {
            "id": "4978aaeb-10c2-4aaf-b048-3ca9e28998d8",
            "fluid": "caffeinated",
            "observed": true,
            "visitId": "b7c54214-8861-4495-a244-fac62abace70",
            "timestamp": "2019-05-12T20:55:36.813Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "56890b44-f575-4d66-840a-b402d19a81e5",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111",
            "consumedVolumeMl": 350
        },
        {
            "id": "f1569947-d390-4cf4-b9ce-bb23c1cc9c85",
            "note": "",
            "visitId": "b7c54214-8861-4495-a244-fac62abace70",
            "timestamp": "2019-05-12T20:55:18.072Z",
            "volumeMl": 600,
            "eventType": "catheter_observation",
            "caregiverId": "56890b44-f575-4d66-840a-b402d19a81e5",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        },
        {
            "id": "f12cc590-5dc8-4194-b694-efc49a593856",
            "meal": "meal",
            "note": "Chicken curry chips and rice .",
            "visitId": "673c8125-0850-4b0b-8a86-972a302b318b",
            "timestamp": "2019-05-12T19:01:48+01:00",
            "eventType": "food_intake_observation",
            "caregiverId": "ac3967a6-1392-4227-9987-a201e0f8f287",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        },
        {
            "id": "eddb3ce8-42e7-48d0-90b6-66ac7e495b28",
            "note": "",
            "visitId": "673c8125-0850-4b0b-8a86-972a302b318b",
            "timestamp": "2019-05-12T18:02:03.493Z",
            "volumeMl": 600,
            "eventType": "catheter_observation",
            "caregiverId": "ac3967a6-1392-4227-9987-a201e0f8f287",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        },
        {
            "id": "36875066-8f6f-4b73-a9ec-0fd086353a1d",
            "fluid": "regular",
            "observed": true,
            "visitId": "673c8125-0850-4b0b-8a86-972a302b318b",
            "timestamp": "2019-05-12T18:01:30.998Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "ac3967a6-1392-4227-9987-a201e0f8f287",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111",
            "consumedVolumeMl": 350
        },
        {
            "id": "4fea4846-ae4b-49ef-9d40-724023d2d275",
            "fluid": "caffeinated",
            "observed": true,
            "visitId": "673c8125-0850-4b0b-8a86-972a302b318b",
            "timestamp": "2019-05-12T18:01:19.654Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "ac3967a6-1392-4227-9987-a201e0f8f287",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111",
            "consumedVolumeMl": 230
        },
        {
            "id": "fdd52a68-cccc-404d-b054-40e612e26df1",
            "mood": "happy",
            "visitId": "5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5",
            "timestamp": "2019-05-12T15:20:15+01:00",
            "eventType": "mood_observation",
            "caregiverId": "868ffde9-b069-4af5-8835-c4ac4e72e4b5",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d"
        },
        {
            "id": "5ee5fba9-ad08-40b5-9b00-ebb9e7046b20",
            "meal": "meal",
            "note": "Chicken korma, cookie and yogurt ",
            "visitId": "5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5",
            "timestamp": "2019-05-12T15:20:05+01:00",
            "eventType": "food_intake_observation",
            "caregiverId": "868ffde9-b069-4af5-8835-c4ac4e72e4b5",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d"
        },
        {
            "id": "3b8fcd8e-c103-4433-98d2-78c737826c9f",
            "fluid": "regular",
            "observed": false,
            "visitId": "5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5",
            "timestamp": "2019-05-12T14:19:50.301Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "868ffde9-b069-4af5-8835-c4ac4e72e4b5",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d",
            "consumedVolumeMl": 230
        },
        {
            "id": "31fa2500-96ec-4067-82d2-728069e138ce",
            "fluid": "regular",
            "observed": false,
            "visitId": "5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5",
            "timestamp": "2019-05-12T14:19:50.156Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "868ffde9-b069-4af5-8835-c4ac4e72e4b5",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d",
            "consumedVolumeMl": 230
        },
        {
            "id": "f0244161-dae5-4eca-8392-39d38cedcfd3",
            "fluid": "regular",
            "observed": false,
            "visitId": "5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5",
            "timestamp": "2019-05-12T14:19:50.012Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "868ffde9-b069-4af5-8835-c4ac4e72e4b5",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d",
            "consumedVolumeMl": 230
        },
        {
            "id": "826af9f1-99d9-4cf3-a69c-dfc07ba87080",
            "fluid": "caffeinated",
            "observed": false,
            "visitId": "5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5",
            "timestamp": "2019-05-12T14:19:45.449Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "868ffde9-b069-4af5-8835-c4ac4e72e4b5",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d",
            "consumedVolumeMl": 230
        },
        {
            "id": "d903e831-cebf-40c2-a0d7-6b7ba30127d1",
            "meal": "snack",
            "note": "3 x biscuits",
            "visitId": "394bb93b-2672-41e6-8562-263fbdb6cc41",
            "timestamp": "2019-05-12T13:30:58+01:00",
            "eventType": "food_intake_observation",
            "caregiverId": "f4cace06-0a06-4e3b-88c9-716b9931bd87",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        },
        {
            "id": "7d2372dc-688c-45c4-a300-f6f1372f6758",
            "meal": "snack",
            "note": "Sandwich cheese and biscuits hot cross bun for lunch ",
            "visitId": "5cd753f0-8b66-f8a8-41e5-e67f68359c5c",
            "timestamp": "2019-05-12T12:55:26+01:00",
            "eventType": "food_intake_observation",
            "caregiverId": "5c9090ab-7d5e-4a72-8bf7-197190ad4c98",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d"
        },
        {
            "id": "81f5099d-4a50-409a-949a-233d443869c8",
            "note": "",
            "visitId": "394bb93b-2672-41e6-8562-263fbdb6cc41",
            "timestamp": "2019-05-12T12:32:46.394Z",
            "volumeMl": 250,
            "eventType": "catheter_observation",
            "caregiverId": "f4cace06-0a06-4e3b-88c9-716b9931bd87",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        },
        {
            "id": "5759d232-5762-42cb-97d4-94411af8d0d2",
            "fluid": "caffeinated",
            "observed": true,
            "visitId": "394bb93b-2672-41e6-8562-263fbdb6cc41",
            "timestamp": "2019-05-12T12:30:44.672Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "f4cace06-0a06-4e3b-88c9-716b9931bd87",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111",
            "consumedVolumeMl": 230
        },
        {
            "id": "658ec002-c687-42ff-a83c-dec858d17936",
            "fluid": "regular",
            "observed": true,
            "visitId": "394bb93b-2672-41e6-8562-263fbdb6cc41",
            "timestamp": "2019-05-12T12:30:34.590Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "f4cace06-0a06-4e3b-88c9-716b9931bd87",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111",
            "consumedVolumeMl": 350
        },
        {
            "id": "7a49485f-806b-451c-9d94-505b65677edf",
            "fluid": "caffeinated",
            "observed": false,
            "visitId": "5cd753f0-8b66-f8a8-41e5-e67f68359c5c",
            "timestamp": "2019-05-12T11:54:29.505Z",
            "eventType": "fluid_intake_observation",
            "caregiverId": "5c9090ab-7d5e-4a72-8bf7-197190ad4c98",
            "careRecipientId": "df50cac5-293c-490d-a06c-ee26796f850d",
            "consumedVolumeMl": 350
        },
        {
            "id": "f515f259-5ac2-423c-9070-0eee6b1681e4",
            "meal": "meal",
            "note": "4 x weetabix with hot milk",
            "visitId": "94eb0f11-e187-4f70-8c21-7cb3ab03ea31",
            "timestamp": "2019-05-12T10:05:59+01:00",
            "eventType": "food_intake_observation",
            "caregiverId": "f4cace06-0a06-4e3b-88c9-716b9931bd87",
            "careRecipientId": "ad3512a6-91b1-4d7d-a005-6f8764dd0111"
        }
    ],
    "totalCount": 938,
    "totalPages": 46.9
}


export const normalizedObservationsMock: NormalizedModel<Observation> = {
    "19f90822-d91f-4c5f-a67c-38e5c87f7e27": {
        id: '19f90822-d91f-4c5f-a67c-38e5c87f7e27',
        meal: 'snack',
        note: '3 x biscuits ',
        visitId: 'b7c54214-8861-4495-a244-fac62abace70',
        timestamp: '2019-05-12T22:05:07+01:00',
        eventType: 'food_intake_observation',
        caregiverId: '56890b44-f575-4d66-840a-b402d19a81e5',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '4978aaeb-10c2-4aaf-b048-3ca9e28998d8': {
        id: '4978aaeb-10c2-4aaf-b048-3ca9e28998d8',
        fluid: 'caffeinated',
        observed: true,
        visitId: 'b7c54214-8861-4495-a244-fac62abace70',
        timestamp: '2019-05-12T20:55:36.813Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '56890b44-f575-4d66-840a-b402d19a81e5',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 350
    },
    'f1569947-d390-4cf4-b9ce-bb23c1cc9c85': {
        id: 'f1569947-d390-4cf4-b9ce-bb23c1cc9c85',
        note: '',
        visitId: 'b7c54214-8861-4495-a244-fac62abace70',
        timestamp: '2019-05-12T20:55:18.072Z',
        volumeMl: 600,
        eventType: 'catheter_observation',
        caregiverId: '56890b44-f575-4d66-840a-b402d19a81e5',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    'f12cc590-5dc8-4194-b694-efc49a593856': {
        id: 'f12cc590-5dc8-4194-b694-efc49a593856',
        meal: 'meal',
        note: 'Chicken curry chips and rice .',
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T19:01:48+01:00',
        eventType: 'food_intake_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    'eddb3ce8-42e7-48d0-90b6-66ac7e495b28': {
        id: 'eddb3ce8-42e7-48d0-90b6-66ac7e495b28',
        note: '',
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T18:02:03.493Z',
        volumeMl: 600,
        eventType: 'catheter_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '36875066-8f6f-4b73-a9ec-0fd086353a1d': {
        id: '36875066-8f6f-4b73-a9ec-0fd086353a1d',
        fluid: 'regular',
        observed: true,
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T18:01:30.998Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 350
    },
    '4fea4846-ae4b-49ef-9d40-724023d2d275': {
        id: '4fea4846-ae4b-49ef-9d40-724023d2d275',
        fluid: 'caffeinated',
        observed: true,
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T18:01:19.654Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 230
    },
    'fdd52a68-cccc-404d-b054-40e612e26df1': {
        id: 'fdd52a68-cccc-404d-b054-40e612e26df1',
        mood: 'happy',
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T15:20:15+01:00',
        eventType: 'mood_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d'
    },
    '5ee5fba9-ad08-40b5-9b00-ebb9e7046b20': {
        id: '5ee5fba9-ad08-40b5-9b00-ebb9e7046b20',
        meal: 'meal',
        note: 'Chicken korma, cookie and yogurt ',
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T15:20:05+01:00',
        eventType: 'food_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d'
    },
    '3b8fcd8e-c103-4433-98d2-78c737826c9f': {
        id: '3b8fcd8e-c103-4433-98d2-78c737826c9f',
        fluid: 'regular',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:50.301Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    '31fa2500-96ec-4067-82d2-728069e138ce': {
        id: '31fa2500-96ec-4067-82d2-728069e138ce',
        fluid: 'regular',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:50.156Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    'f0244161-dae5-4eca-8392-39d38cedcfd3': {
        id: 'f0244161-dae5-4eca-8392-39d38cedcfd3',
        fluid: 'regular',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:50.012Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    '826af9f1-99d9-4cf3-a69c-dfc07ba87080': {
        id: '826af9f1-99d9-4cf3-a69c-dfc07ba87080',
        fluid: 'caffeinated',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:45.449Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    'd903e831-cebf-40c2-a0d7-6b7ba30127d1': {
        id: 'd903e831-cebf-40c2-a0d7-6b7ba30127d1',
        meal: 'snack',
        note: '3 x biscuits',
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T13:30:58+01:00',
        eventType: 'food_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '7d2372dc-688c-45c4-a300-f6f1372f6758': {
        id: '7d2372dc-688c-45c4-a300-f6f1372f6758',
        meal: 'snack',
        note: 'Sandwich cheese and biscuits hot cross bun for lunch ',
        visitId: '5cd753f0-8b66-f8a8-41e5-e67f68359c5c',
        timestamp: '2019-05-12T12:55:26+01:00',
        eventType: 'food_intake_observation',
        caregiverId: '5c9090ab-7d5e-4a72-8bf7-197190ad4c98',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d'
    },
    '81f5099d-4a50-409a-949a-233d443869c8': {
        id: '81f5099d-4a50-409a-949a-233d443869c8',
        note: '',
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T12:32:46.394Z',
        volumeMl: 250,
        eventType: 'catheter_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '5759d232-5762-42cb-97d4-94411af8d0d2': {
        id: '5759d232-5762-42cb-97d4-94411af8d0d2',
        fluid: 'caffeinated',
        observed: true,
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T12:30:44.672Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 230
    }, '658ec002-c687-42ff-a83c-dec858d17936': {
        id: '658ec002-c687-42ff-a83c-dec858d17936',
        fluid: 'regular',
        observed: true,
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T12:30:34.590Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 350
    },
    '7a49485f-806b-451c-9d94-505b65677edf': {
        id: '7a49485f-806b-451c-9d94-505b65677edf',
        fluid: 'caffeinated',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-41e5-e67f68359c5c',
        timestamp: '2019-05-12T11:54:29.505Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '5c9090ab-7d5e-4a72-8bf7-197190ad4c98',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 350
    },
    'f515f259-5ac2-423c-9070-0eee6b1681e4': {
        id: 'f515f259-5ac2-423c-9070-0eee6b1681e4',
        meal: 'meal',
        note: '4 x weetabix with hot milk',
        visitId: '94eb0f11-e187-4f70-8c21-7cb3ab03ea31',
        timestamp: '2019-05-12T10:05:59+01:00',
        eventType: 'food_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    }
}

export const normalizedObservationsMock2: NormalizedModel<Observation> = {
    "219f90822-d91f-4c5f-a67c-38e5c87f7e27": {
        id: '19f90822-d91f-4c5f-a67c-38e5c87f7e27',
        meal: 'snack',
        note: '3 x biscuits ',
        visitId: 'b7c54214-8861-4495-a244-fac62abace70',
        timestamp: '2019-05-12T22:05:07+01:00',
        eventType: 'food_intake_observation',
        caregiverId: '56890b44-f575-4d66-840a-b402d19a81e5',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '24978aaeb-10c2-4aaf-b048-3ca9e28998d8': {
        id: '4978aaeb-10c2-4aaf-b048-3ca9e28998d8',
        fluid: 'caffeinated',
        observed: true,
        visitId: 'b7c54214-8861-4495-a244-fac62abace70',
        timestamp: '2019-05-12T20:55:36.813Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '56890b44-f575-4d66-840a-b402d19a81e5',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 350
    },
    '2f1569947-d390-4cf4-b9ce-bb23c1cc9c85': {
        id: 'f1569947-d390-4cf4-b9ce-bb23c1cc9c85',
        note: '',
        visitId: 'b7c54214-8861-4495-a244-fac62abace70',
        timestamp: '2019-05-12T20:55:18.072Z',
        volumeMl: 600,
        eventType: 'catheter_observation',
        caregiverId: '56890b44-f575-4d66-840a-b402d19a81e5',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '2f12cc590-5dc8-4194-b694-efc49a593856': {
        id: 'f12cc590-5dc8-4194-b694-efc49a593856',
        meal: 'meal',
        note: 'Chicken curry chips and rice .',
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T19:01:48+01:00',
        eventType: 'food_intake_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '2eddb3ce8-42e7-48d0-90b6-66ac7e495b28': {
        id: 'eddb3ce8-42e7-48d0-90b6-66ac7e495b28',
        note: '',
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T18:02:03.493Z',
        volumeMl: 600,
        eventType: 'catheter_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '236875066-8f6f-4b73-a9ec-0fd086353a1d': {
        id: '36875066-8f6f-4b73-a9ec-0fd086353a1d',
        fluid: 'regular',
        observed: true,
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T18:01:30.998Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 350
    },
    '24fea4846-ae4b-49ef-9d40-724023d2d275': {
        id: '4fea4846-ae4b-49ef-9d40-724023d2d275',
        fluid: 'caffeinated',
        observed: true,
        visitId: '673c8125-0850-4b0b-8a86-972a302b318b',
        timestamp: '2019-05-12T18:01:19.654Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'ac3967a6-1392-4227-9987-a201e0f8f287',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 230
    },
    '2fdd52a68-cccc-404d-b054-40e612e26df1': {
        id: 'fdd52a68-cccc-404d-b054-40e612e26df1',
        mood: 'happy',
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T15:20:15+01:00',
        eventType: 'mood_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d'
    },
    '25ee5fba9-ad08-40b5-9b00-ebb9e7046b20': {
        id: '5ee5fba9-ad08-40b5-9b00-ebb9e7046b20',
        meal: 'meal',
        note: 'Chicken korma, cookie and yogurt ',
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T15:20:05+01:00',
        eventType: 'food_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d'
    },
    '23b8fcd8e-c103-4433-98d2-78c737826c9f': {
        id: '3b8fcd8e-c103-4433-98d2-78c737826c9f',
        fluid: 'regular',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:50.301Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    '231fa2500-96ec-4067-82d2-728069e138ce': {
        id: '31fa2500-96ec-4067-82d2-728069e138ce',
        fluid: 'regular',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:50.156Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    '2f0244161-dae5-4eca-8392-39d38cedcfd3': {
        id: 'f0244161-dae5-4eca-8392-39d38cedcfd3',
        fluid: 'regular',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:50.012Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    '2826af9f1-99d9-4cf3-a69c-dfc07ba87080': {
        id: '826af9f1-99d9-4cf3-a69c-dfc07ba87080',
        fluid: 'caffeinated',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-4275-60ef0c0ee1d5',
        timestamp: '2019-05-12T14:19:45.449Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '868ffde9-b069-4af5-8835-c4ac4e72e4b5',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 230
    },
    '2d903e831-cebf-40c2-a0d7-6b7ba30127d1': {
        id: 'd903e831-cebf-40c2-a0d7-6b7ba30127d1',
        meal: 'snack',
        note: '3 x biscuits',
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T13:30:58+01:00',
        eventType: 'food_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '27d2372dc-688c-45c4-a300-f6f1372f6758': {
        id: '7d2372dc-688c-45c4-a300-f6f1372f6758',
        meal: 'snack',
        note: 'Sandwich cheese and biscuits hot cross bun for lunch ',
        visitId: '5cd753f0-8b66-f8a8-41e5-e67f68359c5c',
        timestamp: '2019-05-12T12:55:26+01:00',
        eventType: 'food_intake_observation',
        caregiverId: '5c9090ab-7d5e-4a72-8bf7-197190ad4c98',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d'
    },
    '281f5099d-4a50-409a-949a-233d443869c8': {
        id: '81f5099d-4a50-409a-949a-233d443869c8',
        note: '',
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T12:32:46.394Z',
        volumeMl: 250,
        eventType: 'catheter_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    },
    '25759d232-5762-42cb-97d4-94411af8d0d2': {
        id: '5759d232-5762-42cb-97d4-94411af8d0d2',
        fluid: 'caffeinated',
        observed: true,
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T12:30:44.672Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 230
    }, '2658ec002-c687-42ff-a83c-dec858d17936': {
        id: '658ec002-c687-42ff-a83c-dec858d17936',
        fluid: 'regular',
        observed: true,
        visitId: '394bb93b-2672-41e6-8562-263fbdb6cc41',
        timestamp: '2019-05-12T12:30:34.590Z',
        eventType: 'fluid_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111',
        consumedVolumeMl: 350
    },
    '27a49485f-806b-451c-9d94-505b65677edf': {
        id: '7a49485f-806b-451c-9d94-505b65677edf',
        fluid: 'caffeinated',
        observed: false,
        visitId: '5cd753f0-8b66-f8a8-41e5-e67f68359c5c',
        timestamp: '2019-05-12T11:54:29.505Z',
        eventType: 'fluid_intake_observation',
        caregiverId: '5c9090ab-7d5e-4a72-8bf7-197190ad4c98',
        careRecipientId: 'df50cac5-293c-490d-a06c-ee26796f850d',
        consumedVolumeMl: 350
    },
    '2f515f259-5ac2-423c-9070-0eee6b1681e4': {
        id: 'f515f259-5ac2-423c-9070-0eee6b1681e4',
        meal: 'meal',
        note: '4 x weetabix with hot milk',
        visitId: '94eb0f11-e187-4f70-8c21-7cb3ab03ea31',
        timestamp: '2019-05-12T10:05:59+01:00',
        eventType: 'food_intake_observation',
        caregiverId: 'f4cace06-0a06-4e3b-88c9-716b9931bd87',
        careRecipientId: 'ad3512a6-91b1-4d7d-a005-6f8764dd0111'
    }
}


export const observationsMetaDataMock: ObservationsMetaData = {
    page: 1, itemsPerPage: 20, totalCount: 938, totalPages: 46
}