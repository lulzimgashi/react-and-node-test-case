export enum ObservationEventType {
    CONCERN_RAISED = 'concern_raised',
    CATHETER_OBSERVATION = 'catheter_observation',
    FOOD_INTAKE_OBSERVATION = 'food_intake_observation',
    FLUID_INTAKE_OBSERVATION = 'fluid_intake_observation',
    PHYSICAL_HEALTH_OBSERVATION = 'physical_health_observation',
    MOOD_OBSERVATION = 'mood_observation'
}