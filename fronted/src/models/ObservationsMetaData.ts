
export default class ObservationsMetaData {

    page: number = 0;
    itemsPerPage: number = 20;
    totalCount: number = 0;
    totalPages: number = 0;

}