import { schema } from 'normalizr';

export default class Observation {
    public id?: string = '';
    public careRecipientId?: string = '';
    public caregiverId?: string = '';
    public consumedVolumeMl?: number = 0;
    public volumeMl?: number = 0;
    public eventType?: string = '';
    public fluid?: string = '';
    public mood?: string = '';
    public note?: string = '';
    public meal?: string = '';
    public observed?: boolean = false;
    public timestamp?: string = '';
    public visitId?: string = '';

}

export const ObservationSchema = new schema.Entity('observations');