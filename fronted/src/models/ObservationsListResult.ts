import Observation from './Observation';
import ObservationsMetaData from './ObservationsMetaData';

export default class ObservationsListResult extends ObservationsMetaData {

    public records?: Observation[] = [];

}