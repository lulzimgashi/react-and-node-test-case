import { ObservationEventType } from "../models/enums/ObservationEventType";
import Observation from "../models/Observation";

export default class ObservationUtils {

    public static getEventValue(observation: Observation) {
        switch (observation.eventType) {
            case ObservationEventType.CATHETER_OBSERVATION:
                return observation.note;
            case ObservationEventType.CONCERN_RAISED:
                return observation.note;
            case ObservationEventType.FLUID_INTAKE_OBSERVATION:
                return observation.fluid;
            case ObservationEventType.FOOD_INTAKE_OBSERVATION:
                return observation.meal;
            case ObservationEventType.MOOD_OBSERVATION:
                return observation.mood;
            case ObservationEventType.PHYSICAL_HEALTH_OBSERVATION:
                return observation.note;
            default:
                return '';
        }
    }

    public static generateRandomColor() {

        const randomInt = (min: number, max: number) => {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };

        const h = randomInt(0, 360);
        const s = randomInt(42, 98);
        const l = randomInt(40, 90);
        return `hsl(${h},${s}%,${l}%)`;

    }
}